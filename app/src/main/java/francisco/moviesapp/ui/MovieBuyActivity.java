package francisco.moviesapp.ui;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.wefika.flowlayout.FlowLayout;

import java.util.ArrayList;
import java.util.List;

import francisco.moviesapp.R;
import francisco.moviesapp.databinding.ActivityMovieBuyBinding;

/**
 * Created by Fran on 31/07/2017.
 */

public class MovieBuyActivity extends AppCompatActivity {

    ActivityMovieBuyBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_movie_buy);

        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setTitle("Valores");
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (getIntent().hasExtra("photoPath")){
            Picasso.with(this)
                    .load(getIntent().getExtras().getString("photoPath"))
                    .placeholder(R.drawable.placeholder_photo)
                    .error(R.drawable.placeholder_photo)
                    .into(binding.background);
        }

        initView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void initView(){
        setHorizontalLayout();
    }

    private void setHorizontalLayout() {
        String[] google_play_types = getResources().getStringArray(R.array.google_play_types);
        String[] google_play_prices = getResources().getStringArray(R.array.google_play_prices);
        String[] google_play_buy_rent = getResources().getStringArray(R.array.google_play_buy_rent);

        String[] vudu_types = getResources().getStringArray(R.array.vudu_types);
        String[] vudu_prices = getResources().getStringArray(R.array.vudu_prices);
        String[] vudu_buy_rent = getResources().getStringArray(R.array.vudu_buy_rent);

        int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 100, getResources().getDisplayMetrics());
        int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 100, getResources().getDisplayMetrics());
        FlowLayout.LayoutParams params = new FlowLayout.LayoutParams(width, height);

        final List<RelativeLayout> viewList = new ArrayList<>();

        for (int i = 0; i < google_play_types.length; i++) {
            final RelativeLayout view = (RelativeLayout) LayoutInflater.from(this).inflate(R.layout.item_buy_option, null);
            params.setMargins(16, 16, 16, 16);
            final TextView type = view.findViewById(R.id.buy_title);
            TextView price = view.findViewById(R.id.buy_price);
            TextView buyOrRent = view.findViewById(R.id.buy_text);

            type.setText(google_play_types[i]);
            price.setText(google_play_prices[i]);
            buyOrRent.setText(google_play_buy_rent[i]);

            binding.flowLayoutGooglePlay.addView(view, params);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    changeItemLayout(view, viewList);
                }
            });
            viewList.add(view);
        }

        for (int i = 0; i < vudu_types.length; i++) {
            final RelativeLayout view = (RelativeLayout) LayoutInflater.from(this).inflate(R.layout.item_buy_option, null);
            params.setMargins(16, 16, 16, 16);
            TextView type = view.findViewById(R.id.buy_title);
            TextView price = view.findViewById(R.id.buy_price);
            TextView buyOrRent = view.findViewById(R.id.buy_text);

            type.setText(vudu_types[i]);
            price.setText(vudu_prices[i]);
            buyOrRent.setText(vudu_buy_rent[i]);

            binding.flowLayoutVudu.addView(view, params);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    changeItemLayout(view, viewList);
                }
            });

            viewList.add(view);
        }
    }

    private void changeItemLayout(View view, List<RelativeLayout> viewList){
        for (RelativeLayout item : viewList){
            TextView type = item.findViewById(R.id.buy_title);
            TextView price = item.findViewById(R.id.buy_price);
            TextView buyOrRent = item.findViewById(R.id.buy_text);
            ImageView background = item.findViewById(R.id.background);

            type.setTextColor(ContextCompat.getColor(MovieBuyActivity.this, R.color.white));
            price.setTextColor(ContextCompat.getColor(MovieBuyActivity.this, R.color.white));
            buyOrRent.setTextColor(ContextCompat.getColor(MovieBuyActivity.this, R.color.white));
            background.setAlpha(0.5f);
        }

        TextView type = view.findViewById(R.id.buy_title);
        TextView price = view.findViewById(R.id.buy_price);
        TextView buyOrRent = view.findViewById(R.id.buy_text);
        ImageView background = view.findViewById(R.id.background);

        type.setTextColor(ContextCompat.getColor(MovieBuyActivity.this, R.color.colorPrimaryDark));
        price.setTextColor(ContextCompat.getColor(MovieBuyActivity.this, R.color.colorPrimaryDark));
        buyOrRent.setTextColor(ContextCompat.getColor(MovieBuyActivity.this, R.color.colorPrimaryDark));
        background.setAlpha(1f);

        binding.flBuyButton.setBackgroundColor(ContextCompat.getColor(MovieBuyActivity.this, R.color.colorPrimary));
    }
}
