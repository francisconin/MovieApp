package francisco.moviesapp.ui;

import android.arch.lifecycle.LifecycleFragment;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import francisco.moviesapp.R;
import francisco.moviesapp.databinding.FragmentMoviesListBinding;
import francisco.moviesapp.extras.RecyclerViewClickListener;
import francisco.moviesapp.model.Movie;
import francisco.moviesapp.model.ResponsePopularMovies;
import francisco.moviesapp.viewmodel.MoviesViewModel;

/**
 * Created by Fran on 27/07/2017.
 */

public class MoviesListFragment extends LifecycleFragment implements RecyclerViewClickListener {

    FragmentMoviesListBinding binding;
    MoviesViewModel moviesViewModel;
    MoviesAdapter mAdapter;
    GridLayoutManager layoutManager;

    private boolean loading = true;
    private int pastVisiblesItems;
    private int visibleItemCount;
    private int totalItemCount;

    private int pageNumber = 1;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_movies_list, container, false);
        setRetainInstance(true);

        moviesViewModel = ViewModelProviders.of(getActivity()).get(MoviesViewModel.class);

        binding.rvList.setHasFixedSize(true);

        layoutManager = new GridLayoutManager(getContext(), 3);
        binding.rvList.setLayoutManager(layoutManager);

        List<Movie> list = new ArrayList<>();
        mAdapter = new MoviesAdapter(getContext(), list, this);
        binding.rvList.setAdapter(mAdapter);
        binding.rvList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy)
            {
                if(dy > 0)
                {
                    visibleItemCount = layoutManager.getChildCount();
                    totalItemCount = layoutManager.getItemCount();
                    pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();

//                    if (loading)
//                    {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount)
                        {
//                            loading = false;
                            if (moviesViewModel.getIsFavoriteFragment()){
                                loadFavoriteMovies();
                            } else {
                                loadPopularMovies();
                            }
                        }
//                    }
                }
            }
        });

        if (moviesViewModel.getIsFavoriteFragment()){
            loadFavoriteMovies();
        } else {
            loadPopularMovies();
        }

        return binding.getRoot();
    }

    @Override
    public void recyclerViewListClicked(View v, int position) {
        Intent intent = new Intent(getActivity(), MovieDetailsActivity.class);
        intent.putExtra("movie", mAdapter.getItem(position));
        startActivity(intent);
    }

    private void loadPopularMovies(){
        moviesViewModel.getPopularMovies(getString(R.string.backend_movie_api_key), pageNumber,
                binding.progressBar).observe(this, new Observer<ResponsePopularMovies>() {
            @Override
            public void onChanged(@Nullable ResponsePopularMovies responsePopularMovies) {
                if (responsePopularMovies != null) {
                    binding.tvTitle.setText(getString(R.string.title_movies));
                    binding.rlTopLayout.setVisibility(View.VISIBLE);
                    pageNumber = responsePopularMovies.getPage() + 1;
                    mAdapter.addNewList(responsePopularMovies.getMovies());
                    mAdapter.notifyDataSetChanged();
                }
            }
        });
    }

    private void loadFavoriteMovies(){
        moviesViewModel.getFavoritedMoviesList().observe(this, new Observer<List<Movie>>() {
            @Override
            public void onChanged(@Nullable List<Movie> movies) {
                if (movies != null){
                    binding.tvTitle.setText(getString(R.string.title_favorites));
                    binding.rlTopLayout.setVisibility(View.VISIBLE);
                    mAdapter.replaceItems(movies);
                    mAdapter.notifyDataSetChanged();
                }
            }
        });
    }
}