package francisco.moviesapp.ui;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import francisco.moviesapp.R;
import francisco.moviesapp.extras.RecyclerViewClickListener;
import francisco.moviesapp.model.Movie;

/**
 * Created by Fran on 27/07/2017.
 */

public class MoviesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<Movie> mList;
    private static RecyclerViewClickListener itemListener;

    public MoviesAdapter(Context context, List<Movie> list, RecyclerViewClickListener itemListener) {
        this.mList = list;
        this.context = context;
        MoviesAdapter.itemListener = itemListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_movie, viewGroup, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        Movie item = mList.get(position);
        ItemViewHolder viewHolder = (ItemViewHolder) holder;

        viewHolder.tvTitle.setText(item.getTitle());
        viewHolder.tvReleaseDate.setText(item.getReleaseYear());
        String photoPath = context.getString(R.string.base_url_image) + item.getPosterPath();
        Picasso.with(context)
                .load(photoPath)
                .placeholder(R.drawable.placeholder_photo)
                .error(R.drawable.placeholder_photo)
                .into(viewHolder.ivPhotoPath);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public void replaceItems(List<Movie> list){
        mList = list;
    }

    public void addListItem(Movie b, int position) {
        mList.add(position, b);
        notifyItemInserted(position);
    }

    public void addNewList(List<Movie> newList){
        mList.addAll(newList);
    }

    public void addList(List<Movie> list){
        mList = list;
    }

    public Movie getItem(int position){
        return mList.get(position);
    }

    public List<Movie> getList(){
        return mList;
    }

    private class ItemViewHolder extends RecyclerView.ViewHolder implements RecyclerView.OnClickListener{

        ImageView ivPhotoPath;
        TextView tvTitle;
        TextView tvReleaseDate;

        private ItemViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);

            ivPhotoPath = itemView.findViewById(R.id.iv_movie_picture);
            tvTitle = itemView.findViewById(R.id.tv_movie_title);
            tvReleaseDate = itemView.findViewById(R.id.tv_movie_year);
        }

        @Override
        public void onClick(View v) {
            itemListener.recyclerViewListClicked(v, this.getLayoutPosition());
        }
    }
}
