package francisco.moviesapp.ui;

import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.LifecycleRegistryOwner;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import francisco.moviesapp.R;
import francisco.moviesapp.databinding.ActivityMovieDetailsBinding;
import francisco.moviesapp.model.Movie;
import francisco.moviesapp.viewmodel.MoviesViewModel;

/**
 * Created by Fran on 28/07/2017.
 */

public class MovieDetailsActivity extends AppCompatActivity implements LifecycleRegistryOwner {
    LifecycleRegistry lifecycleRegistry = new LifecycleRegistry(this);

    ActivityMovieDetailsBinding binding;

    Movie movie;

    MoviesViewModel moviesViewModel;

    @Override
    public LifecycleRegistry getLifecycle() {
        return lifecycleRegistry;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_movie_details);

        moviesViewModel = ViewModelProviders.of(this).get(MoviesViewModel.class);

        if (getIntent().hasExtra("movie")){
            movie = getIntent().getExtras().getParcelable("movie");
            initView();
        } else {
            finish();
            Toast.makeText(this, getString(R.string.gerenic_error_app), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void initView(){
        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setTitle(movie.getTitle());
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        binding.tvMovieTitle.setText(movie.getTitle());
        binding.tvMovieYear.setText(movie.getReleaseYear());
        binding.tvOverview.setText(movie.getOverview());

        checkMovieFavorites();

        String photoPath = getString(R.string.base_url_image) + movie.getPosterPath();
        Picasso.with(this)
                .load(photoPath)
                .placeholder(R.drawable.placeholder_photo)
                .error(R.drawable.placeholder_photo)
                .into(binding.ivMoviePicture);

        binding.llAddRemoveFavorites.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (movie.isFavorite()){
                    moviesViewModel.deleteFavoritedMovie(movie);
                } else {
                    moviesViewModel.insertNewFavoritedMovie(MovieDetailsActivity.this.movie);
                }
            }
        });

        binding.flBuyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MovieDetailsActivity.this, MovieBuyActivity.class);
                String photoPath = getString(R.string.base_url_image) + movie.getPosterPath();
                intent.putExtra("photoPath", photoPath);
                startActivity(intent);
            }
        });
    }

    private void checkMovieFavorites(){
        moviesViewModel.getFavoritedMovieById(String.valueOf(movie.getId())).observe(this, new Observer<Movie>() {
            @Override
            public void onChanged(@Nullable Movie movie) {
                if (movie != null){
                    binding.ivFavoriteStar.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_star_black_24dp, null));
                    MovieDetailsActivity.this.movie.setFavorite(true);
                } else {
                    binding.ivFavoriteStar.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_star_border_black_24dp, null));
                    MovieDetailsActivity.this.movie.setFavorite(false);
                }
            }
        });
    }
}
