package francisco.moviesapp.di;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import francisco.moviesapp.R;
import francisco.moviesapp.data.APIService;
import francisco.moviesapp.data.AppRepository;
import francisco.moviesapp.data.Repository;
import francisco.moviesapp.data.SharedPrefsHelper;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Fran on 27/07/2017.
 */

@Module
public class AppModule {

    private final Application application;

    public AppModule(Application application) {
        this.application = application;
    }

    @Provides
    @Singleton
    public APIService providesApiService(){
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        return new Retrofit.Builder()
                .baseUrl(APIService.PATH_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
                .create(APIService.class);
    }

    @Provides
    @Singleton
    public SharedPreferences providesPrefs(){
        return application.getSharedPreferences(application.getString(R.string.prefs_name), Context.MODE_PRIVATE);
    }

    @Provides
    @Singleton
    public SharedPrefsHelper providesPrefsHelper(SharedPreferences prefs) {
        return new SharedPrefsHelper(prefs);
    }

    @Provides
    @Singleton
    public AppRepository providesRepository(APIService apiService, SharedPrefsHelper prefsHelper){
        return new Repository(application, apiService, prefsHelper);
    }
}
