package francisco.moviesapp.di;

import android.content.Context;
import android.support.annotation.NonNull;

import javax.inject.Singleton;

import dagger.Component;
import francisco.moviesapp.ui.HomeActivity;
import francisco.moviesapp.ui.MoviesListFragment;
import francisco.moviesapp.ui.MovieDetailsActivity;
import francisco.moviesapp.viewmodel.MoviesViewModel;

/**
 * Created by Fran on 27/07/2017.
 */

@Component(modules = AppModule.class)
@Singleton
public abstract class AppComponent {

    public static AppComponent from (@NonNull Context context){
        return ((MainApplication) context.getApplicationContext()).getAppComponent();
    }

    public abstract void inject(HomeActivity homeActivity);
    public abstract void inject(MoviesViewModel moviesViewModel);
    public abstract void inject(MoviesListFragment moviesListFragment);
    public abstract void inject(MovieDetailsActivity movieDetailsActivity);
}
