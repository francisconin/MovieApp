package francisco.moviesapp.data;


import francisco.moviesapp.model.ResponsePopularMovies;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Fran on 27/07/2017.
 */

public interface APIService {
    String PATH_URL = "https://api.themoviedb.org/3/";

    @GET("movie/popular?language=pt-BR")
    Call<ResponsePopularMovies> getPopularMovies(@Query("api_key") String apiKey, @Query("page") int pageNumber);
}
