package francisco.moviesapp.data;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.persistence.room.Room;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import francisco.moviesapp.R;
import francisco.moviesapp.db.AppDatabase;
import francisco.moviesapp.model.Movie;
import francisco.moviesapp.model.ResponsePopularMovies;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Fran on 27/07/2017.
 */

@Singleton
public class Repository implements AppRepository {

    private APIService apiService;
    private SharedPrefsHelper prefsHelper;
    private Application application;
    private AppDatabase mDatabase;

    @Inject
    public Repository(Application application, APIService apiService, SharedPrefsHelper prefsHelper) {
        this.application = application;
        this.apiService = apiService;
        this.prefsHelper = prefsHelper;

        mDatabase = Room.databaseBuilder(application, AppDatabase.class, application.getString(R.string.database_name)).build();
    }

    @Override
    public LiveData<ResponsePopularMovies> getPopularMovies(String apiKey, int page, final ProgressBar progressBar) {
        progressBar.setVisibility(View.VISIBLE);
        final MutableLiveData<ResponsePopularMovies> liveData = new MutableLiveData<>();
        apiService.getPopularMovies(apiKey, page).enqueue(new Callback<ResponsePopularMovies>() {
            @Override
            public void onResponse(@NonNull Call<ResponsePopularMovies> call, @NonNull Response<ResponsePopularMovies> response) {
                if (response.isSuccessful()){
                    liveData.setValue(response.body());
                } else {
                    Toast.makeText(application, application.getText(R.string.rest_generic_error), Toast.LENGTH_LONG).show();
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NonNull Call<ResponsePopularMovies> call, @NonNull Throwable t) {
                connectionFailed();
                progressBar.setVisibility(View.GONE);
            }
        });

        return liveData;
    }

    @Override
    public LiveData<List<Movie>> getFavoritedMoviesList() {
        return mDatabase.movieDAO().getMoviesList();
    }

    @Override
    public void insertNewFavorite(Movie movie) {
        new InsertMovieAsync().execute(movie);
    }

    @Override
    public void deleteFavorite(Movie movie) {
        new DeleteMovieAsync().execute(movie);
    }

    @Override
    public LiveData<Movie> getFavoritedMovieById(String id){
        return mDatabase.movieDAO().getMovieById(id);
    }

    private class InsertMovieAsync extends AsyncTask<Movie, Void, Void> {
        @Override
        protected Void doInBackground(Movie... params) {
            mDatabase.movieDAO().insert(params[0]);
            return null;
        }
    }

    private class DeleteMovieAsync extends AsyncTask<Movie, Void, Void> {
        @Override
        protected Void doInBackground(Movie... params) {
            mDatabase.movieDAO().delete(params[0]);
            return null;
        }
    }

    private void connectionFailed(){
        Toast.makeText(application, application.getText(R.string.rest_no_connection), Toast.LENGTH_LONG).show();
    }
}
