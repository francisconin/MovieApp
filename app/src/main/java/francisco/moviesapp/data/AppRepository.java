package francisco.moviesapp.data;

import android.arch.lifecycle.LiveData;
import android.widget.ProgressBar;

import java.util.List;

import francisco.moviesapp.model.Movie;
import francisco.moviesapp.model.ResponsePopularMovies;

/**
 * Created by Fran on 27/07/2017.
 */

public interface AppRepository {
    LiveData<ResponsePopularMovies> getPopularMovies (String apiKey, int page, ProgressBar progressBar);

    LiveData<Movie> getFavoritedMovieById (String id);

    LiveData<List<Movie>> getFavoritedMoviesList();

    void insertNewFavorite(Movie movie);

    void deleteFavorite(Movie movie);
}
