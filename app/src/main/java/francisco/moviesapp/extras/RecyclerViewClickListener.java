package francisco.moviesapp.extras;

import android.view.View;

/**
 * Created by Fran on 27/07/2017.
 */

public interface RecyclerViewClickListener {
    void recyclerViewListClicked(View v, int position);
}
