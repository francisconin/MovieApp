package francisco.moviesapp.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import francisco.moviesapp.R;
import francisco.moviesapp.model.Movie;

/**
 * Created by Fran on 27/07/2017.
 */

@Database(entities = {Movie.class}, version = 7, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase INSTANCE;

    public static AppDatabase getDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE =
                    Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, context.getString(R.string.database_name))
                            .build();
        }
        return INSTANCE;
    }

    public abstract MovieDAO movieDAO();

}
