package francisco.moviesapp.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import francisco.moviesapp.model.Movie;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by Fran on 27/07/2017.
 */

@Dao
public interface MovieDAO {
    @Query("select * from Movie")
    LiveData<List<Movie>> getMoviesList();

    @Query("select * from Movie where id = :id")
    LiveData<Movie> getMovieById(String id);

    @Insert(onConflict = REPLACE)
    void insert(Movie movie);

    @Delete
    void delete(Movie movie);

    @Update(onConflict = REPLACE)
    void update(Movie movie);
}
