package francisco.moviesapp.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.LiveData;
import android.widget.ProgressBar;

import java.util.List;

import javax.inject.Inject;

import francisco.moviesapp.data.AppRepository;
import francisco.moviesapp.di.MainApplication;
import francisco.moviesapp.model.Movie;
import francisco.moviesapp.model.ResponsePopularMovies;

/**
 * Created by Fran on 27/07/2017.
 */

public class MoviesViewModel extends AndroidViewModel implements LifecycleObserver {

    private boolean isFavoriteFragment;

    @Inject
    AppRepository repository;

    public MoviesViewModel(Application application) {
        super(application);
        ((MainApplication) application).getAppComponent().inject(this);
    }

    public LiveData<ResponsePopularMovies> getPopularMovies (String apiKey, int pageNumber, ProgressBar progressBar){
        return repository.getPopularMovies(apiKey, pageNumber, progressBar);
    }

    public LiveData<Movie> getFavoritedMovieById (String id){
        return repository.getFavoritedMovieById(id);
    }

    public LiveData<List<Movie>> getFavoritedMoviesList (){
        return repository.getFavoritedMoviesList();
    }

    public void setIsFavoriteFragment(boolean state){
        isFavoriteFragment = state;
    }

    public boolean getIsFavoriteFragment () {
        return isFavoriteFragment;
    }

    public void insertNewFavoritedMovie(Movie movie){
        repository.insertNewFavorite(movie);
    }

    public void deleteFavoritedMovie(Movie movie){
        repository.deleteFavorite(movie);
    }
}
